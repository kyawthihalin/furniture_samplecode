@if ($paginator->hasPages())
<nav class="blog-pagination justify-content-center d-flex">
    <ul class="pagination">
        @if ($paginator->onFirstPage())
            <li class="page-item disabled">
                <a class="page-link"><i class="ti-angle-left"></i></a>
            </li>
        @else
            <li class="page-item ">
                <a href="{{ $paginator->previousPageUrl() }}" class="disabled page-link" aria-label="Previous">
                        <i class="ti-angle-left">
                    </i>
                </a>
            </li>
        @endif

        @foreach ($elements as $element)
           
            @if (is_string($element))
                <li class="page-item disabled">
                    <a class="page-link">{{ $element }}</a>
                </li>
            @endif


           
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active">
                            <a  class="disabled page-link" aria-label="Previous">
                            {{ $page }}
                            </a>
                        </li>
                    @else
                        <li class="page-item ">
                            <a href="{{ $url }}" class="disabled page-link" aria-label="Previous">
                            {{ $page }}
                            </a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach


        
        @if ($paginator->hasMorePages())
            <li class="page-item ">
                <a href="{{ $paginator->nextPageUrl() }}" class="disabled page-link" aria-label="Previous">
                <i class="ti-angle-right"></i>
                </a>
            </li>
        @else
            <li class="page-item disabled">
                <a class="page-link"><i class="ti-angle-right"></i></a>
            </li>
        @endif
    </ul>
</nav>
@endif 