@extends('../layout.frontapp')
@section('main')
@php
use App\Models\Product;
use App\Models\Category;
$highlights = Product::orderBy('id','desc')->take('4')->get();
$f_designs  = Category::where('status','funiture_design')->orderBy('id','desc')->take('3')->get();
@endphp


    <main>
        <!-- 2022 COLLECTION Area Start-->
        <section class="category-area">
            <div class="container-fluid enchant-space">
                <div class="row text-center">
                    <div class="col-12">
                        <img src="{{asset('assets/img/logo/logo-collec.png')}}" alt="" class="logo-collection">
                    </div>
                </div>
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mt-coll mb-30">
                            <h2>ENCHANT 2022 COLLECTION</h2>
                            <p>Discover a world possibilities</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center collect-1-bg pb-3">
                    <div class="col-xl-8 col-lg-8">
                        <div class="single-category">
                            <div class="category-img">
                                <img src="{{asset('assets/img/collection/main.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8 text-center pb-5">
                        <h3 class="pb-3"> BROWSE OUR LATEST PROJECTS</h3>
                        <a href="/showcase" class="black-btn">Discover</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- 2022 COLLECTION Area End-->

        <!-- COLLECTION SLIDE Area Start-->
        <section class="slide-hightlight">
            <div class="container-fluid">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-60">
                            <h2>COLLECTION HIGHLIGHTS</h2>
                            <p>Discover a world possibilities</p>
                        </div>
                    </div>
                </div>
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="container-fluid">
                                <div class="row pl-4 pr-3">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="single-product">
                                            <div class="product-img px-0">
                                                <img src="{{asset('assets/img/collection/coll-2.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Benson Table Lamp</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-1.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4"> 
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-3.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4"> 
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="container-fluid">
                                <div class="row pl-4 pr-3">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-2.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Benson Table Lamp</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-1.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-3.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> -->
                </div>
            </div>
        </section>
        <!-- COLLECTION SLIDE Area End-->

        <!-- COLLECTION Area Start-->
        <section class="category-area section-padding30 black-bg">
            <div class="container-fluid">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle-1 text-center mb-25">
                            <h2>LIVIING ROOM INSPIRATION</h2>
                            <p>Discover a world possibilities</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center pb-5 px-5">
                    <div class="col-xl-12 col-lg-12">
                        <div class="single-category mb-55">
                            <div class="category-img">
                                <img src="{{asset('assets/img/collection/main2.jpg')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle-1 text-center mb-25">
                            <h3>Bedroom Inspiration</h3>
                            <p>Discover a world possibilities</p>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center px-5">
                    <div class="col-xl-6 col-lg-6 text-center pb-5">
                        <div class="single-category mb-30">
                            <div class="category-img">
                                <img src="{{asset('assets/img/collection/sub-main01.png')}}" alt="">
                            </div>
                        </div>
                       
                        <a href="http://enchant-mm.com/sub/bed_stand" class="black-btn-01">Discover</a>
                    </div>
                    <div class="col-xl-6 col-lg-6 text-center pb-5">
                        <div class="single-category mb-30">
                            <div class="category-img">
                                <img src="{{asset('assets/img/collection/sub-main02.png')}}" alt="">
                            </div>
                        </div>
                        <a href="http://enchant-mm.com/sub/sofa" class="black-btn-01">Discover</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- COLLECTION Area End-->

        <!-- 4 slide COLLECTION Area Start-->
        <section class="slide-hightlight">
            <div class="container-fluid">
                <!-- Section Tittle -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-tittle text-center mb-45">
                            <h2>WHAT THE COMMUNITY IS LOVING</h2>
                        </div>
                    </div>
                </div>
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="container-fluid">
                                <div class="row justify-content-center enchant-small-space">
                                    <div class="col">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="assets/img/collection/collection-1.png" alt="" class="img-fluid">
                                            </div>
                                            <div class="product-caption pt-5">
                                                <div class="price">
                                                    <ul>
                                                        <li>Benson Table Lamp</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="assets/img/collection/collection-2.png" alt="" class="img-fluid">
                                            </div>
                                            <div class="product-caption pt-5">
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="assets/img/collection/collection-3.png" alt="" class="img-fluid">
                                            </div>
                                            <div class="product-caption pt-5">
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="assets/img/collection/collection-4.png" alt="" class="img-fluid">
                                            </div>
                                            <div class="product-caption pt-5">
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="assets/img/collection/collection-3.png" alt="" class="img-fluid">
                                            </div>
                                            <div class="product-caption pt-5">
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">100000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>--}}
                </div>
            </div>
        </section>
        <!-- 4 slide COLLECTION Area End-->
       
        <!-- Browse Area Start-->
        <section class="browse-collection" data-background="{{asset('assets/img/collection/browse.jpg')}}">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 browse-content col-11">
                        <h1>Our Factory</h1>
                        <a href="/factory" class="black-btn mt-4">Learn more</a>
                    </div>
                </div>
            </div>
        </section>
        <!-- Browse Area End-->
                        
        <!-- ENQUIRE Area Start-->
        <section class="container-fluid category-area section-padding30">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center">
                        <h2>Complement Your Space with Our Contemporary Furnitures</h2>
                        <p>
                            We work closely with you to make sure your project is a successful and you get the results you want with a bit of magic 
                        </p>
                    </div>
                </div>
            </div>
            <div class="row justify-content-md-center">
                <div class="col-6">
                    <div class="single-category mb-30">
                        <div class="category-img">
                            <img src="{{asset('assets/img/collection/enquire1.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="single-category mb-30">
                        <div class="category-img">
                            <img src="{{asset('assets/img/collection/enquire2.png')}}" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 text-center">
                    <a href="/contact-us" class="black-btn">Enquire Now</a>
                </div>
            </div>
        </section>

        <!-- ENQUIRE Area Start-->

        <!-- some highly used PRODUCTS  Area Start-->
        <section class="container category-area pt-50">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center pb-4">
                        <h2>SOME HIGHLY USED PRODUCTS</h2>
                        
                    </div>
                </div>
            </div>    
            <div class="row">
                <div class="col-md-4 col-4">
                    <div class="single-category mb-30">
                        <div>
                            <img src="assets/img/product/prod-1.png" alt="" class="img-fluid">
                        </div>
                        <div class="text-center mt-3">
                            <a href="http://enchant-mm.com/sub/bed_stand" class="text-dark">Bed</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-4">
                    <div class="single-category mb-30">
                        <div>
                            <img src="assets/img/product/prod-2.png" alt="" class="img-fluid">
                        </div>
                        <div class="text-center mt-3">
                            <a href="http://enchant-mm.com/sub/sofa" class="text-dark">Office</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-4">
                    <div class="single-category mb-30">
                        <div>
                            <img src="assets/img/product/prod-3.png" alt="" class="img-fluid">
                        </div>
                        <div class="text-center mt-3">
                            <a href="http://enchant-mm.com/sub/dining_table" class="text-dark">Living Room</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- some highly used PRODUCTS  Area End-->

        <!-- partner area section start-->
        <section class="category-area section-padding30">
            <div class="container customer-bg">
                <div class="row justify-content-center pt-5">
                    <div class="col-md-12">
                        <div class="section-tittle text-center">
                            <h2>OUR CUSTOMERS</h2>
                        </div>
                    </div>
                    <div class="col-10">
                        <div class="row">
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="">
                                        <img src="{{asset('assets/img/partner/partner-1.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="">
                                        <img src="{{asset('assets/img/partner/partner-2.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="">
                                        <img src="{{asset('assets/img/partner/partner-3.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="">
                                        <img src="{{asset('assets/img/partner/partner-4.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="">
                                        <img src="{{asset('assets/img/partner/partner-5.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-4 col-md-2">
                                <div class="single-category mb-30">
                                    <div class="category-img">
                                        <img src="{{asset('assets/img/partner/partner-6.png')}}" alt="" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                        </div>
                   </div> 
                </div>
            </div>
        </section>
        <!-- partner area section end-->

        <!-- Contact us start -->
        <section class="browse-collection" data-background="{{asset('assets/img/collection/contact.png')}}">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 text-center contact-padding">
                        <img src="{{asset('assets/img/logo/logo-contact.png')}}" alt="" class="img-fluid pb-4">
                        <!-- <h4 class="white-color">ENCHANT</h4> -->
                        <h1 class="white-color browse-title">ENCHANT FURNITURES</h1> 
                        <p class="white-color pt-3 pb-4">Bring a unique touch to your home with a Enchant Said consultation</p>
                        <div class="d-none d-sm-block">
                            <a href="/contact-us" class="white-btn">Contact Us</a>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- Contact us start -->



    </main>
@endsection