@extends('../layout.app')
@section('main')

<main>
    <div class="about-enchant enchant-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center">We are enchant</h4>
                    <div class="about-sub">Discover a world of possibilities</div>
                    <p class="text-center" style="font-size: 18px;"> 
                        We work closely with you to make sure your project is a successful and you get the results you want with a bit of magic

                    </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center about-us-img">
            <div class="col-md-8">
                    <img src="assets/img/about.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="container about-us">
       <div class="row">
            <div class="col-md-6 bg-about-us">
                <h5>Package Quotations </h5>
                <hr>
                <p>
                    Enchant team provides budgeted quotations with different design packages to match our customer’s budget. Our talented designers will work closely with you to make sure your project is a successful and you get the results you want with a bit of magic.
                </p>
            </div>
            <div class="col-md-6 px-0">
               <img src="assets/img/mission.jpg" alt="" class="img-fluid"> 
            </div>
        
            <div class="col-md-6 px-0">
               <img src="assets/img/vission.jpg" alt="" class="img-fluid"> 
            </div>
            <div class="col-md-6 bg-about-us">
                <h5>Project Proposals</h5>
                <hr>
                <p>
                   The experience of Enchant Luxury Furniture’s team allows us to create a furniture proposal with designs in accordance with your interior design. Our broad array of furniture designs help you bring your interior imagination to life.
                </p>    
            </div>
          

            <div class="col-md-6 bg-about-us">
                <h5>Full House Furnish</h5>
                <hr>
                <p>
                    We take pride in our success of completing full house furnishing. We cater to your design and provide upmost luxury in everything including accessories, paintings, lamps, carpets and more.
                </p>
            </div>
            <div class="col-md-6 px-0">
               <img src="assets/img/mission.jpg" alt="" class="img-fluid"> 
            </div>
        
            <div class="col-md-6 px-0">
               <img src="assets/img/vission.jpg" alt="" class="img-fluid"> 
            </div>
            <div class="col-md-6 bg-about-us">
                <h5>Delivery and Setup</h5>
                <hr>
                <p>
                    Our company provides flexible delivery on all our products and professionally install all our items at desired locations in your home. We emphasize on luxury quality and make sure our delivered items are in the best conditions, otherwise we take responsibility.
                </p>    
            </div>
        </div> 
    </div>
 </main>

@endsection