@php
use App\Models\ProductSize;
use App\Models\ProductColor;
    $product_sizes = ProductSize::where('product_id',$product->id)->get();
    $product_colors = ProductColor::where('product_id',$product->id)->get();
@endphp

@extends('../layout.app')
@section('main')
<main>
    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container pb-4 pt-85">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                   <div class="single_product_pics">
                        <div class="row">
                            <div class="col-lg-12 image_col">
                                <div class="single_product_image">
                                    <div class="single_product_image_background" style="background-image:url({{url($product->fimage)}})"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 thumbnails_col">
                                <div class="single_product_thumbnails">
                                    <ul>
                                        @foreach($product->image as $image)
                                        <li><img src="{{asset($image)}}" alt="" data-image="{{asset($image)}}"></li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-6">
                    <div class="single_product_text">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 style="font-size:30px;font-weight:bold;">{{$product->name}}</h4>
                                <p>Discover a world of possibilities</p>
                            </div>
                            <div class="price pb-2 d-none d-sm-block">
                                <h5>
                                    @php 
                                        $currency = App\Models\Currency::find($product->currency_id);
                                        $currency_sign = $currency->sign;
                                    @endphp
                                    @if($currency_sign != "MMK")
                                    {{$currency_sign." ".$product->price}}
                                    @else
                                    {{$product->price}} {{$currency_sign}}
                                    @endif
                                </h5>
                            </div>
                        </div>
 
                        <div class="description">
                            {!! $product->description !!}
                        </div>
                        
                        <p class="pb-5">Product ID - {{$product->product_uid}}</p>
                        
                        <div class="card_area">
                            <h6 class="mb-4" style="font-size:20px!important;text-transform:unset;">Size:</h6>
                            <div>
                            @foreach($product_sizes as $single_product)
                            @if($single_product->size != null)
                                <a href="#" class="btn_size">{{$single_product->size->name}}</a>
                            @else
                                <a href="#" class="btn_size">No Available Sizes</a>
                            @endif
                            @endforeach
                            </div>
                            <div class="py-4"></div>
                            <h6 class="mb-4" style="font-size:20px!important;text-transform:unset;">Color:</h6>
                            @foreach($product_colors as $color)
                            @if($color->color_id != null)
                            <div style="width:20px;background-color:{{$color->color->color_code}};float:left" height="20px" class="ml-2 mr-2 mb-4">
                                &nbsp;
                            </div>
                            @else 
                            @endif
                            @endforeach
                            <div class="clearfix"></div>
                           
                            </div>
                            <div class="add_to_cart pt-3 d-none d-sm-block">
                                <a href="/contact-us" class="black-btn">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->
    
    <!-- COLLECTION SLIDE Area Start-->
    <section class="slide-hightlight-01">
        <div class="container-fluid">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center mb-85">
                        <h2>you may also like</h2>
                        <p>Discover a world possibilities</p>
                    </div>
                </div>
            </div>
             <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <div class="container-fluid">
                                <div class="row pl-4 pr-3">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="single-product">
                                            <div class="product-img px-0">
                                                <img src="{{asset('assets/img/collection/coll-2.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Benson Table Lamp</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-1.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4"> 
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-60">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-3.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4"> 
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="container-fluid">
                                <div class="row pl-4 pr-3">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-2.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Benson Table Lamp</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-1.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Megan Marble Object</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-12 d-none d-sm-block">
                                        <div class="single-product mb-30">
                                            <div class="product-img">
                                                <img src="{{asset('assets/img/collection/coll-3.png')}}" alt="" class="heightlight-space">
                                            </div>
                                            <div class="product-caption pt-4">
                                                <div class="price">
                                                    <ul>
                                                        <li>Chelsea Bronze Planter</li>
                                                    </ul>
                                                </div>
                                                <h4><a href="#">200000 MMK</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> -->
                </div>
        </div>
    </section>
    <!-- COLLECTION SLIDE Area End-->
</main>
@endsection