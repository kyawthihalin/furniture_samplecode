@extends('../layout.app')
@section('main')

<main>
    <div class="about-enchant enchant-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center">We are enchant</h4>
                    <div class="about-sub">Discover a world of possibilities</div>
                    <p>
                        Enchant is a high-end furniture company that represents top grade furniture product designs from Itlay and Germany.

                    </p>
                    <p>
                        Our OEM factory esports high end furniture to more than 100 countries worldwide, while the manfacturing process and quality control are governed by the International Quality Control Department.
                    </p>
                    <p>
                        We specialize in loose furniture such as Mattresses, Sofa, Bed-stands Tables, Chairs, and related accessories

                    </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center about-us-img">
            <div class="col-md-8">
                    <img src="assets/img/fac-main.jpg" alt="" class="img-fluid">
            </div>
        </div>
    </div>

    <div class="container about-us">
       <div class="row">
            
            <div class="col-md-6 bg-about-us">
                <h5>OUR MISSION</h5>
                <hr>
                <p>
                    Our mission is to make sure that our clients receives only the best products and to experience comfort through trust of our company.
                </p>
            </div>
            <div class="col-md-6 px-0">
               <img src="assets/img/mission.jpg" alt="" class="img-fluid  about-us-mobile"> 
            </div>
        
            <div class="col-md-6 px-0 d-none d-sm-block">
               <img src="assets/img/vission.jpg" alt="" class="img-fluid"> 
            </div>
            <div class="col-md-6 bg-about-us">
                <h5>OUR VISSION</h5>
                <hr>
                <p>
                    Enchant’s vision is to become a one stop solution for everyone to achieve a first-class interior.
                </p>    
            </div>
            <div class="col-md-6 px-0 d-block d-sm-none">
               <img src="assets/img/vission.jpg" alt="" class="img-fluid about-us-mobile"> 
            </div>
        </div> 
    </div>
 </main>

@endsection