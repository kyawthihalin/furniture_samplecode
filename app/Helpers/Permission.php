<?php

namespace App\Helpers;

class Permission{
    public static function setPermissions($permission, $crud){
        $crud->denyAccess(['list', 'create', 'update', 'delete', 'show']);
    
        // Allow list access
        if (backpack_user()->can('list-'.$permission)) {
            $crud->allowAccess('list');
        }
    
        // Allow create access
        if (backpack_user()->can('create-'.$permission)) {
            $crud->allowAccess('create');
        }
    
        // Allow update access
        if (backpack_user()->can('update-'.$permission)) {
            $crud->allowAccess('update');
        }
    
        // Allow delete access
        if (backpack_user()->can('delete-'.$permission)) {
            $crud->allowAccess('delete');
        }
    }
}

