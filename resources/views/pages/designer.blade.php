@extends('../layout.app')
@section('main')
<main class="enchant-bg">

    <!-- slider Area Start-->
    <div class="slider-area ">
        <!-- Mobile Menu -->
        <div class="single-slider slider-height2 d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12 pt-5">
                        <div class="designer-cap text-center pt-5 pb-4">
                            <h3>TEAM ENCHANT</h3>
                            <p class="pt-2">The way we work together determines the way we succeed.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- slider Area End-->

    <!-- Latest Products Start -->
    <section class="team pb-5">
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 col-md-5 pt-3">
                    <img src="assets/img/designer/designer-1.jpg" alt="" class="designer-img"> 
                </div>
                <div class="col-12 col-md-7 pt-4">
                    <p>Chief Executive Officer (CEO)</p>
                    <div class="headline"></div>
                    <h4 class="pt-4">Mario Bellini</h4>
                    <p>
                       He has received the Golden Compass award eight times and numberless of other prestigious architecture awards. His designed projects include the Milan Portello Trade Fair, Tokyo Design Centre in Japan, National gallery of Victoria in Melbourne, the USA Natuzzi , German Essen Expo Centre, Paris Art Louvre Department of Islamic, and Europe largest new Milan Convention Centre.
                    </p>
                   
                    <ul class="list-unstyled">      
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>  
            </div>
            <div class="row py-5">
                <div class="col-12 col-md-5 d-block d-sm-none">
                    <img src="assets/img/designer/designer-2.jpg" alt="" class="designer-img"> 
                </div> 
                <div class="col-12 col-md-7 pt-4">
                    <p>Chief Executive Officer (CEO)</p>
                    <div class="headline"></div>
                    <h4 class="pt-4">Saar Zafrir</h4>
                    <p>
                        Saar Zafrir is a famous furniture designer in Europe, he cooperated with B&M casa since 2009. He has a good reputation in designing modern furniture and also have studio offers design, investment and development services for hospitality projects throughout Europe. An interior designer by heart, he has involved Europe’s hottest design hotel properties including the beautiful guest rooms of max brown hotels and Sir Savigny Berlin along with the full design of Brown Beach House Croatia and Provocateur Berlin and Golden Phoneix Restaurant.
                    </p>
                    
                    <ul class="list-unstyled">      
                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div> 
                <div class="col-12 col-md-5 d-none d-sm-block pt-4">
                    <img src="assets/img/designer/designer-2.jpg"" alt="" class="designer-img"> 
                </div> 
            </div>
        </div>
    </section>
    <!-- Latest Products End -->
</main>
@endsection