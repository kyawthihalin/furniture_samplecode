@php
use App\Models\ProductSize;
use App\Models\ProductColor;
    $product_sizes = ProductSize::where('product_id',$product->id)->get();
    $product_colors = ProductColor::where('product_id',$product->id)->get();
@endphp

@extends('../layout.app')
@section('main')
<main>
    <!--================Single Product Area =================-->
    <div class="product_image_area">
        <div class="container pb-4 pt-85">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="product_img_slide owl-carousel">
                        @foreach($product->image as $image)
                        <div class="single_product_img">
                            <img src="{{asset($image)}}" alt="#" class="img-fluid">
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single_product_text">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 style="font-size:30px">{{$product->name}}</h4>
                                <p>Discover a world of possibilities</p>
                            </div>
                            <div class="price pb-2 d-none d-sm-block">
                                <h5>${{$product->price}}</h5>
                            </div>
                        </div>
 
                        <div class="description">
                            {!! $product->description !!}
                        </div>
                        
                        <p class="pb-3">Product ID - {{$product->product_uid}}</p>
                        
                        <div class="card_area">
                            <h6 class="mb-4" style="font-size:20px!important;text-transform:unset;">Size:</h6>
                            <div>
                                @foreach($product_sizes as $single_product)
                                    <a href="#" class="btn_size">{{$single_product->size->name}}</a>
                                @endforeach
                            </div>
                            <div class="py-4"></div>
                            <h6 class="mb-4" style="font-size:20px!important;text-transform:unset;">Color:</h6>
                            @foreach($product_colors as $color)
                            <div style="width:20px;background-color:{{$color->color->color_code}};float:left" height="20px" class="ml-2 mr-2 mb-4">
                                &nbsp;
                            </div>
                            @endforeach
                            <div class="clearfix"></div>
                           
                            </div>
                            <div class="add_to_cart pt-3 d-none d-sm-block">
                                <a href="#" class="black-btn">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--================End Single Product Area =================-->
    
    <!-- COLLECTION SLIDE Area Start-->
    <section class="slide-hightlight">
        <div class="container-fluid">
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-tittle text-center mb-85">
                        <h2>you may also like</h2>
                        <p>Discover a world possibilities</p>
                    </div>
                </div>
            </div>
            <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="container">
                            <div class="row">
                                @foreach($product_you_may_know as $pymk)
                                <a href="{{route('single',$pymk->id)}}">
                                <div class="col-4">
                                    <div class="single-product">
                                        <div class="product-img">
                                            <img src="{{asset($pymk->fimage)}}" alt="" class="img-fluid">
                                        </div>
                                        <div class="product-caption pt-5">
                                            <div class="price">
                                                <ul>
                                                    <li>{{$pymk->name}}</li>
                                                </ul>
                                            </div>
                                            <h4><a href="#">${{$pymk->price}}</a></h4>
                                        </div>
                                    </div>
                                </div>
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- COLLECTION SLIDE Area End-->
</main>
@endsection