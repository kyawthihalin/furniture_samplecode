@extends('../layout.app')
@section('main')

<main class="enchant-bg brand-space"> 
  <!-- slider Area Start-->
    <div class="slider-area ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 pt-5">
                    <div class="designer-cap text-center pt-5 pb-4">
                        <h3>PROJECT NAME</h3>
                        <p class="pt-2">Lorem ipsum dolor sit ame</p>
                    </div>
                    <!-- <div class="px-space">
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip  ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolorfugiat nulla pariatur. Excepteur sint occaecat cupidatat non proiden.</p>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip  ex ea commodo consequat</p> 
                    </div> -->
                </div>
            </div>
        </div> 
    </div>
    <!-- slider Area End-->

    <div class="gallery js-flickity" data-flickity-options='{ "wrapAround": true }'>
      <div class="gallery-cell">
        <img src="{{asset('assets/img/slide2.jpg')}}" alt="" class="heightlight-space img-fluid">

      </div>
      <div class="gallery-cell">
        <img src="{{asset('assets/img/slide2.jpg')}}" alt="" class="heightlight-space img-fluid">

      </div>
      <div class="gallery-cell">
        <img src="{{asset('assets/img/slide2.jpg')}}" alt="" class="heightlight-space img-fluid">

      </div>
      <div class="gallery-cell">
        <img src="{{asset('assets/img/slide2.jpg')}}" alt="" class="heightlight-space img-fluid">
      </div>
    </div>
</main>


  

@endsection