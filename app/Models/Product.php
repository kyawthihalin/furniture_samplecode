<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\ProductSize;
use App\Models\ProductCategory;
use App\Models\ProductSubcategory;
use App\Models\ProductColor;
use App\Models\ProductImage;
use Illuminate\Support\Carbon;

class Product extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'image' => 'array',
    ];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    public static function deleteProductVaries($row)
    {
        $product_subcategories = ProductSubcategory::where('product_id',$row->id)->get();
        if($product_subcategories)
        {
            foreach($product_subcategories as $product_subcategory){
                $product_subcategory->delete();
            }
        }

        $product_sizes = ProductSize::where('product_id',$row->id)->get();
        if($product_sizes)
        {
            foreach($product_sizes as $product_size){
                $product_size->delete();
            }
        }

        $product_colors = ProductColor::where('product_id',$row->id)->get();
        if($product_colors){
            foreach($product_colors as $product_color){
                $product_color->delete();
            }
        }
        $null_colors = ProductColor::whereNull('color_id')->get();
        if($null_colors){
            foreach($null_colors as $null_color){
                $null_color->delete();
            }
        }
    }

    public static function saveProductVaries($request,$row,$status)
    {
        if(isset($request->sizes)){
            $product_sizes = $request->sizes;
            foreach($product_sizes as $product_size){
                $create_object_product_size = new ProductSize;
                $create_object_product_size->product_id = $row->id;
                $create_object_product_size->size_id    = $product_size;
                if($status == "created")
                {
                    $create_object_product_size->created_at = Carbon::now();
                }
                $create_object_product_size->updated_at = Carbon::now();
                $create_object_product_size->save();
            }
        }
        

        //creating product sub categories
        if(isset($request->subcategories)){
            $product_subcategories = $request->subcategories;
            foreach($product_subcategories as $product_subcategory)
            {
                $create_object_product_subcategory = new ProductSubcategory;
                $create_object_product_subcategory->product_id = $row->id;
                $create_object_product_subcategory->subcategory_id = $product_subcategory;
                if($status == "created")
                {
                    $create_object_product_subcategory->created_at = Carbon::now();
                }
                $create_object_product_subcategory->updated_at = Carbon::now();
                $create_object_product_subcategory->save();
            }
        }
        //creating product colors
        if(isset($request->colors)){
            $product_colors = $request->colors;
            foreach($product_colors as $product_color)
            {
                $create_object_product_color = new ProductColor;
                $create_object_product_color->product_id = $row->id;
                $create_object_product_color->color_id = $product_color;
                if($status == "created")
                {
                    $create_object_product_color->created_at = Carbon::now();
                }
                $create_object_product_color->updated_at = Carbon::now();
                $create_object_product_color->save();
            }
        }
      
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function categories(){
        return $this->belongsToMany('\App\Models\Category','product_categories');
    }

    public function subcategories(){
        return $this->belongsToMany('\App\Models\Subcategory','product_subcategories');
    }

    public function sizes(){
        return $this->belongsToMany('\App\Models\Size','product_sizes');
    }

    public function colors(){
        return $this->belongsToMany('\App\Models\Color','product_colors');
    }

    public function product_images(){
        return $this->belongsToMany('\App\Models\ProductImage','product_images');
    }

    public function currency(){
        return $this->belongsTo('\App\Models\Currency');
    }


 
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        static::created(function($row){
            //get form data from request
            $request = request();
            //creating product sizes
            self::saveProductVaries($request,$row,"created");
        });

        static::updating(function($row){
            //get form data from request
            $request = request();
            self::deleteProductVaries($row);
            //creating product varies
            self::saveProductVaries($request,$row,"updating");
        });

        static::deleting(function ($row) {
            if (count((array) $row->image)) {
                foreach ($row->image as $file_path) {
                    \Storage::disk('local_public')->delete($file_path);
                }
            }
            \Storage::disk('local_public')->delete($row->fimage);
            self::deleteProductVaries($row);
        });
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */

    public function setFimageAttribute($value)
    {
        $attribute_name = "fimage";
        $disk = "local_public";
        $destination_path = "uploads/images/products";
        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
    }
    public function setImageAttribute($value)
    {
        $attribute_name = "image";
        $disk = "local_public";
        $destination_path = "uploads/images/products";
        $this->uploadMultipleFilesToDisk($value, $attribute_name, $disk, $destination_path);
    }

}
