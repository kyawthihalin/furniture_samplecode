<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->double('price')->default(0);
            $table->double('disprice')->default(0);
            $table->longText('description')->nullable();
            $table->longText('image')->nullable();
            $table->longText('fimage')->nullable();
            $table->double('in_stock')->default(0);
            $table->tinyInteger('status')->default(0);
            // $table->integer('highlight_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
