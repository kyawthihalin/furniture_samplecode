<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductSubcategory;
use App\Models\Subcategory;

class CategoryController extends Controller
{
    // public function getCategory($slug)
    // {
    //    $getCategory = Category::where('slug',$slug)->first();
    //    $products = ProductCategory::where('category_id',$getCategory->id)->paginate(12);
    //    return view('pages.category',compact('products','slug'));
    // }

    public function getSubcategory($slug)
    {
       $getCategory = Subcategory::where('slug',$slug)->first();
       $products = ProductSubcategory::where('subcategory_id',$getCategory->id)->paginate(12);
       return view('pages.category',compact('products','slug','getCategory'));
    }

    public function getSingleProduct($id)
    {
        $product = Product::find($id);
        $product_category = ProductSubcategory::where('product_id',$id)->pluck('subcategory_id')->toArray();
        $products = ProductSubcategory::whereIn('subcategory_id',$product_category)->where('product_id','!=',$id)->pluck('product_id')->unique()->take(3)->toArray();
        $product_you_may_know = Product::whereIn('id',$products)->get();
        return view('pages.single_product',compact('product','product_you_may_know'));
    }
}

