<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Helpers\Permission;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Product::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/product');
        CRUD::setEntityNameStrings('product', 'products');
        Permission::setPermissions('product', $this->crud);
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'Select Date Range'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'created_at', '>=', $dates->from);
                $this->crud->addClause('where', 'created_at', '<=', $dates->to);
            }
        );

        CRUD::addFilter(
            [
                'type' => 'text',
                'name' => 'name',
                'label' => 'Type Product Name'
            ],
            false,
            function ($value) {
                $this->crud->addClause('where', function ($q) use ($value) {
                    return $q->orWhere('name', 'LIKE', "%{$value}%");
                });
            }
        );
        $this->crud->addFilter([
            'name'  => 'in_stock',
            'type'  => 'dropdown',
            'label' => 'Check Stock'
        ], [
            1 => 'In stock',
            2 => 'Out of Stock/Less Stock',
        ], function ($value) { // if the filter is active
            if ($value == 1) {
                $this->crud->addClause('where', 'in_stock', '>', 5);
            } else {
                $this->crud->addClause('where', 'in_stock', '<=', 5);
            }
        });
        $this->crud->addFilter([
            'name'  => 'status',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            1 => 'PRODUCT ON',
            0 => 'PRODUCT OFF',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'status', $value);
        });
    }
    public function showDetailsRow($id)
    {
        $product = Product::find($id);
        return view('partials.products.productDetails', ['product' => $product]);
    }
    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->disableResponsiveTable();
        $this->crud->enableExportButtons();
        CRUD::addColumn([
            'label' => 'Product ID',
            'name'  => 'product_uid',
            'type'  => 'text'
        ]);
        CRUD::column('name');
        CRUD::addColumn(
            [
                // any type of relationship
                'name'         => 'currency', // name of relationship method in the model
                'type'         => 'relationship',
                'label'        => 'Currency', // Table column heading
                // OPTIONAL
                'entity'    => 'currency', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => App\Models\Currency::class, // foreign key model
            ]
        );
        CRUD::addColumn([
            'name'  => 'in_stock',
            'label' => 'Stock Balance',
            'type'  => 'closure',
            'function' => function ($entry) {
                if ($entry->in_stock > 5) {
                    return "<span class='text-success'>$entry->in_stock</span>";
                } else {
                    return "<span class='text-danger'>$entry->in_stock</span>";
                }
            }
        ]);

        CRUD::addColumn([
            'name'  => 'status',
            'label' => 'Product Status',
            'type'  => 'closure',
            'function' => function ($entry) {
                if ($entry->status) {
                    return "<span class='badge badge-success badge-pill'>Active</span>";
                } else {
                    return "<span class='badge badge-danger badge-pill'>Inactive</span>";
                }
            }
        ]);


        CRUD::addColumn([
            'label' => 'Product Price',
            'name'  => 'price',
            'type'  => 'number'
        ]);

        CRUD::addColumn([
            'label' => 'Product Image',
            'name'  => 'fimage',
            'type'  => 'image'
        ]);


        CRUD::column('created_at');
        CRUD::column('updated_at');

        $this->crud->enableDetailsRow();
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductRequest::class);

        CRUD::field('name');
        CRUD::addField([
            'name' => 'product_uid',
            'type' => 'text',
            'label' => 'Product ID',
        ]);

        CRUD::addField(
            [  // Select2
                'label'     => "Currency",
                'type'      => 'select2',
                'name'      => 'currency_id', // the db column for the foreign key
                // optional
                'entity'    => 'currency', // the method that defines the relationship in your Model
                'model'     => "App\Models\Currency", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
            ],
        );
        CRUD::addField([
            'name' => 'price',
            'type' => 'number',
            'label' => 'Price',
        ]);

        CRUD::addField([
            'name'  => 'description',
            'label' => 'Description',
            'type'  => 'summernote',
        ]);
        CRUD::addField([
            'name'      => 'fimage',
            'label'     => 'Feature Image',
            'type'      => 'upload',
            'upload'    => true,
            'disk' => 'local_public'
        ]);
        CRUD::addField([
            'name'      => 'image',
            'label'     => 'Image(Please One or More Images)',
            'type'      => 'upload_multiple',
            'upload'    => true,
            'disk' => 'local_public'
        ]);
        // CRUD::addField([
        //     'label'     => "Choose Categories",
        //     'type'      => 'select2_multiple',
        //     'name'      => 'categories', // the method that defines the relationship in your Model
        //     // optional
        //     'entity'    => 'categories', // the method that defines the relationship in your Model
        //     'model'     => "App\Models\Category", // foreign key model
        //     'attribute' => 'name', // foreign key attribute that is shown to user
        //     'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
        //     'select_all' => true, // show Select All and Clear buttons?
        // ]);

        CRUD::addField([
            'label'     => "Choose Sub Categories",
            'type'      => 'select2_multiple',
            'name'      => 'subcategories', // the method that defines the relationship in your Model
            // optional
            'entity'    => 'subcategories', // the method that defines the relationship in your Model
            'model'     => "App\Models\Subcategory", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
        ]);
        // CRUD::addField([
        //     'label'                      => 'Select2_grouped',
        //     'type'                       => 'select2_grouped', //https://github.com/Laravel-Backpack/CRUD/issues/502
        //     'name'                       => 'subcategories',
        //     'entity'                     => 'subcategories',
        //     'attribute'                  => 'name',
        //     'group_by'                   => 'category', // the relationship to entity you want to use for grouping
        //     'group_by_attribute'         => 'name', // the attribute on related model, that you want shown
        //     'group_by_relationship_back' => 'subcategories', // relationship from related model back to this model
        //     'multiple'                   => true
        // ]);

        CRUD::addField([
            'label'     => "Choose Sizes",
            'type'      => 'select2_multiple',
            'name'      => 'sizes', // the method that defines the relationship in your Model
            // optional
            'entity'    => 'sizes', // the method that defines the relationship in your Model
            'model'     => "App\Models\Size", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
        ]);
        CRUD::addField([
            'label'     => "Choose Colors",
            'type'      => 'select2_multiple',
            'name'      => 'colors', // the method that defines the relationship in your Model
            // optional
            'entity'    => 'colors', // the method that defines the relationship in your Model
            'model'     => "App\Models\Color", // foreign key model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'select_all' => true, // show Select All and Clear buttons?
        ]);
        CRUD::addField([
            'name' => 'in_stock',
            'type' => 'number',
            'label' => 'Stock Balance',
        ]);
        CRUD::addField([
            'name' => 'status',
            'type' => 'checkbox',
            'label' => 'Product Active/Inactive',
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
