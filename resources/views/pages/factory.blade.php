@extends('../layout.app')
@section('main')

<main>
    <div class="about-enchant enchant-bg">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center">B&M FACTORY</h4>
                    <div class="about-sub">Discover a world of possibilities</div>
                    <p>
                        The factory in Foshan, China, covers an astonishing area of 130,000 sqm with more than 1400 employees. The 4 production lines include hardware furniture, panel furniture, solid wood furniture, and upholstery furniture.
                    </p>
                    <p>
                       B&M Casa exports 100-150 containers per month, with an annual output value of around 200 million USD. Enchant is the only authorized distributor of B&M Casa in Myanmar.
                    </p>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center about-us-img">
            <div class="col-md-8">
                    <img src="assets/img/collection/browse.jpg" alt="" class="img-fluid">
                    
            </div>
        </div>
    </div>

    <div class="container about-us">
       <div class="row">
            
            <div class="col-md-6 bg-about-us">
                <h5>OUR MISSION</h5>
                <hr>
                <p>
                    Our mission is to make sure that our clients receives only the best products and to experience comfort through trust of our company.
                </p>
            </div>
            <div class="col-md-6 px-0">
               <img src="assets/img/fac-mission.jpg" alt="" class="img-fluid  about-us-mobile"> 
            </div>
        
            <div class="col-md-6 px-0 d-none d-sm-block">
               <img src="assets/img/fac-vission.jpg" alt="" class="img-fluid"> 
            </div>

            <div class="col-md-6 bg-about-us">

                <h5>OUR VISION</h5>
                <hr>

                <p>
                    Enchant’s vision is to become a one stop solution for everyone to achieve a first-class interior.
                </p>   

            </div>
            <div class="col-md-6 px-0 d-block d-sm-none">
               <img src="assets/img/fac-vission.jpg" alt="" class="img-fluid about-us-mobile"> 
            </div>
        </div> 
    </div>
 </main>

@endsection