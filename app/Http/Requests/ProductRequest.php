<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return backpack_auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'currency_id' => 'required',
            'price' => 'required|min:0',
            'description' => 'required',
            'status'=>'required',
            'product_uid' => 'required',
            'in_stock' => 'required|min:0'
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            //
        ];
    }

    /**
     * Get the validation messages that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'The Product Name is required.',
            'currency_id.required' => 'The Currency is required.',
            'price.required' => 'The Product Price is required.',
            'description.required' => 'The Product Description is required.',
            'image.required' => 'The Product image is required.',
            'fimage.required' => 'The Product Featured image is required.',
            'subcategories.required' => 'The Product subcategory is required.',
            'sizes.required' => 'The Product sizes image is required.',
            'colors.required' => 'The Product colors image is required.',
            'status.required' => 'The Product status image is required.',
            'product_uid.required' => 'The Product ID is required.',
            'in_stock.required' => 'The Product Stock Quantity is required.',
        ];
    }
}
