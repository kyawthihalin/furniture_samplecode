<?php

use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Support\Str;

$categories = Category::where('status', 'navigation')->orderBy('id','DESC')->get();

?>
<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>ENCHANT</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.ico')}}">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
</head>

<body>

    <!-- Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="{{asset('assets/img/logo/logo.png')}}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->

    <header>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header ">
                <div class="header-bottom bg-transparent">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        
                        <div class="col d-none d-lg-block">
                            <!-- Main-menu -->
                            <div class="main-menu float-right">
                                <nav>
                                    <ul id="navigation" class="nav-bar">
                                        <li> </li>
                                        <li><a href="#">PRODUCT</a>
                                            <ul class="submenu">
                                                @foreach($categories as $category)
                                                @php
                                                $subcategories = Subcategory::where('category_id',$category->id)->get();
                                                @endphp
                                                <li class="categories-menu">
                                                    <p class="sub-heading pt-3"> {{$category->name}} </p>
                                                    @foreach($subcategories as $sub)
                                                    <a href="{{route('subcategory',$sub->slug)}}" style="text-decoration:none;"> {{$sub->name}}</a>
                                                    @endforeach
                                                </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li><a href="#">COLLECTION</a></li>
                                        <li><a href="/service">SERVICE</a></li>
                                        <li><a href="/showcase">PROJECT</a></li>

                                        <div class="d-block d-lg-none">
                                            <li><a href="/designer">DESIGNER</a></li>
                                            <li><a href="/factory"> FACTORY</a></li>
                                            <li><a href="/about-us ">ABOUT US</a></li>
                                            <li><a href="/contact-us">CONTACT US</a></li>
                                        </div>
                                    </ul>
                                </nav>
                            </div>

                        </div>
                        <div class="col-1 logo-space">
                            <div class="logo">
                                <a href="/">
                                    <img src="{{asset('assets/img/logo/logow.png')}}" alt="" class="d-none d-sm-block">
                                    <img src="{{asset('assets/img/logo/mobile-logo.png')}}" alt="" class="d-block d-sm-none">
                                </a>
                            </div>
                        </div>
                        <div class="col d-none d-lg-block">

                            <div class="main-menu d-none d-lg-block float-left">
                                <nav>
                                    <ul id="navigation d-block d-sm-none" class="nav-bar">
                                        <li><a href="/designer">DESIGNER</a></li>
                                        <li><a href="/factory"> FACTORY</a></li>
                                        <li><a href="/about-us ">ABOUT US</a></li>
                                        <li><a href="/contact-us">CONTACT US</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>

    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner hero-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('assets/img/hero/slide1.jpg')}}" alt="First slide">
                        <div class="carousel-number d-none d-sm-block">
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"><i class="ti-arrow-left"></i></a> <span class="mx-2"><sup class="pl-3">1</sup> &frasl; <sub class="pr-3">4</sub></span> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('assets/img/hero/slide2.jpg')}}" alt="Second slide">
                        <div class="carousel-number d-none d-sm-block">
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"><i class="ti-arrow-left"></i></a> <span class="mx-2"><sup class="pl-3">2</sup> &frasl; <sub class="pr-3">4</sub></span> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('assets/img/hero/slide3.jpg')}}" alt="Third slide">
                        <div class="carousel-number d-none d-sm-block">
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"><i class="ti-arrow-left"></i></a> <span class="mx-2"><sup class="pl-3">3</sup>&frasl; <sub class="pr-3">4</sub></span> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="{{asset('assets/img/hero/slide4.jpg')}}" alt="Fourth slide">
                        <div class="carousel-number d-none d-sm-block">
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev"><i class="ti-arrow-left"></i></a> <span class="mx-2"><sup class="pl-3">4</sup>&frasl; <sub class="pr-3">4</sub></span> <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next"> <i class="ti-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Hero -->

    @yield('main')
    <footer>

        <a href="https://www.facebook.com" class="facebook d-block d-sm-none">
            <div class="section-fb align-middle" data-background="{{asset('assets/img/facebook.jpeg')}}">
                <div class="icon">
                    <i class="fab fa-facebook-f"></i> Facebook 
                </div>
                <div class="disc pt-4">
                    Brasilia by Marcio Kogan /studio mk27 design
                </div>
            </div>
        </a>

        <!-- Footer Start-->
        <div class="footer-area footer-padding">
            <div class="container">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-3 col-sm-6">
                        <div class="single-footer-caption mb-50">
                            <div class="single-footer-caption mb-30">
                                <!-- logo -->
                                <div class="footer-logo">
                                    <a href="index.html"><img src="{{asset('assets/img/logo/logofooter.png')}}" alt=""></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-5">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Address</h4>
                                <ul>
                                    <li>No. 753/754,</li>
                                    <li>Pinlon Road, 30 Quarter,</li>
                                    <li>North Dagon Township,</li>
                                    <li>Yangon, Myanmar, 11421</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-7">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle">
                                <h4>Work inquiries</h4>
                                <ul>
                                    <li>Interested in working with us?</li>
                                    <li>sales@enchantmm.com</li>
                                </ul>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-7">
                        <div class="single-footer-caption mb-50">
                            <div class="footer-tittle subscribe_part">
                                <h4>Stay In Touch</h4>
                                <div class="subscribe_form">
                                    <input type="email" placeholder="Email address">
                                    <a href="#" class="btn_1">Sign Up</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Footer bottom -->
                <hr>
                <div class="row">
                    <div class="col-xl-7 col-lg-7 col-md-7">
                        <div class="footer-copy-right">
                            <p>
                                Copyright &copy;<script>
                                    document.write(new Date().getFullYear());
                                </script>, Enchant Furnitures</a>
                            </p>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-5 col-md-5">
                        <div class="footer-copy-right f-right">
                            <p>Privacy & Cookie Policy | Terms of Service</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer End-->
    </footer>

    <!-- JS here -->

    <!-- All JS Custom Plugins Link Here here -->
    <script src="{{asset('assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="{{asset('assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{asset('assets/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!-- Jquery Mobile Menu -->
    <script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('assets/js/slick.min.js')}}"></script>

    <!-- One Page, Animated-HeadLin -->
    <script src="{{asset('assets/js/wow.min.js')}}"></script>
    <script src="{{asset('assets/js/animated.headline.js')}}"></script>
    <script src="{{asset('assets/js/jquery.magnific-popup.js')}}"></script>

    <!-- Scrollup, nice-select, sticky -->
    <script src="{{asset('assets/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('assets/js/jquery.sticky.js')}}"></script>

    <!-- contact js -->
    <script src="{{asset('assets/js/contact.js')}}"></script>
    <script src="{{asset('assets/js/jquery.form.js')}}"></script>
    <script src="{{asset('assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/js/mail-script.js')}}"></script>
    <script src="{{asset('assets/js/jquery.ajaxchimp.min.js')}}"></script>

    <!-- Jquery Plugins, main Jquery -->
    <script src="{{asset('assets/js/plugins.js')}}"></script>
    <script src="{{asset('assets/js/main.js')}}"></script>
</body>

</html>