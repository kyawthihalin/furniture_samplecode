@extends('../layout.app')
@section('main')
    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="showcase-container">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="{{asset('assets/img/showcase/banner.png')}}" alt="First slide">
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Hero -->

     <!-- 2022 COLLECTION Area Start-->
    <section class="category-area">
        <div class="container-fluid enchant-space">
            <div class="row text-center">
                <div class="col-12">
                    <img src="{{asset('assets/img/logo/logo-collec.png')}}" alt="" class="logo-collection">
                </div>
            </div>
            <!-- Section Tittle -->
            <div class="row">
                <div class="col-lg-8 offset-md-2">
                    <div class="section-tittle text-center mt-coll mb-30">
                        <p class="showcase-des">
                            Enchant is the home of luxury furniture and interiors, offering a unique experience from product discovery through to delivery; with customised design options and distinctively impressive portfolio of projects and the highest customer service standard. We help clients not only with the vision and development of their space but also making it a reality by transforming empty walls into living, breathing environments that truly express your unique personality and style.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- 2022 COLLECTION Area End-->

    <section class="getintouch">
        <div class="container customer-bg">
            <div class="row text-center py-5">
                <div class="col-12">
                    <h4 class="pb-4">Get In touch with us!</h4>
                    <p>
                         Don’t see what you like? Don’t fret - we have an endless aisle of designs 
                    </p>
                    <p>
                        from our network of suppliers that can provide you with what you need.
                    </p>
                    <p>
                        Just email us at sales@enchantmm.com or <a href="/contact-us">click here</a> 
                    </p>
                    <p>
                        Plus, buy more and save more with our corporate discounts for bulk purchases!
                    </p>
                </div>
            </div>
        </div>
    </section>

     <!-- partner area section start-->
    <section class="category-area team-padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 pt-5">
                    <div class="section-tittle text-center">
                        <h2>OUR CLIENTS</h2>
                    </div>
                </div>
                 <div class="col-12 px-5">
                    <div class="row">
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-1.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-2.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-3.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-4.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-5.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-6.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-1.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-2.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-3.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-4.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-5.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="col-6 col-md-2 d-none d-sm-block">
                            <div class="single-category mb-30">
                                <div class="category-img">
                                    <img src="{{asset('assets/img/partner/partner-6.png')}}" alt="" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr> 
            </div>
            <hr>
        </div>
    </section>
    <!-- partner area section end-->

    <!-- some highly used PRODUCTS  Area Start-->
    <section class="container category-area">
        <!-- Section Tittle -->
        <div class="row">
            <div class="col-lg-12">
                <div class="section-tittle text-center pb-3">
                    <h2>Most Popular</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-1.jpg" alt="">
                    </div>
                    <p class="text-center pt-3">AYA BANKS</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-2.png" alt="">
                    </div>
                    <p class="text-center pt-3">Pun Hlaing</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-3.png" alt="">
                    </div>
                    <p class="text-center pt-3">UAB</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-4.jpg" alt="">
                    </div>
                    <p class="text-center pt-3">CB BANKS</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-5.png" alt="">
                    </div>
                    <p class="text-center pt-3">Golden City</p>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="single-category mb-30">
                    <div class="category-img">
                        <img src="assets/img/showcase/show-6.png" alt="">
                    </div>
                    <p class="text-center pt-3">Shwe Taung</p>
                </div>
            </div>
        </div>
    </section>
    <!-- some highly used PRODUCTS  Area End-->

    <!-- Shop Method Start-->
    <div class="shop-method-area pt-5">
        <div class="container customer-bg mb-2">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="single-method">
                        <img src="assets/img/showcase/icon1.png" alt="" class="img-fluid">
                        <h6 class="my-3">Competitive pricig,extensive selection</h6>
                        <p>
                            With over hundreds products in our catalogue, comprising both funiture and furnishings, rest assured that we can cover all your business needs to a geat value.
                        </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="single-method">
                        <img src="assets/img/showcase/icon2.png" alt="">
                        <h6 class="my-3">Dedicated account manager</h6>
                        <p>One to one support ready with furniture styling advice, selection, order management and delivery coordination. </p>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4">
                    <div class="single-method">
                        <img src="assets/img/showcase/icon3.png" alt="">
                        <h6 class="my-3">On site consultation</h6>
                        <p>Always get the most suitable interior furnishing advice after a visit to your project site.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Shop Method End-->

@endsection