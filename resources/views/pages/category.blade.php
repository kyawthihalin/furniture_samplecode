@extends('../layout.app')
@section('main')
<main>

<!-- slider Area Start-->
<div class="slider-area ">
    <!-- Mobile Menu -->
    <div class="single-slider d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="hero-cap text-center">
                        <h2 style="text-transform: uppercase;">{{$getCategory->name}}</h2>
                        <p class="pb-3">Discover a world of possibilities</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider Area End-->

<!-- Latest Products Start -->
<section class="latest-product-area">
    <div class="container">
        <div class="row">
            @foreach($products as $product)
            <div class="col-xl-3 col-lg-3 col-md-4 col-6 pt-1">
                <a href="{{route('single',$product->product->id)}}">
                <div class="single-product mb-60">
                    <div class="product-img">
                        <img src="{{asset($product->product->fimage)}}" alt="">
                    </div>
                    <div class="product-caption">
                        <h4><a href="{{route('single',$product->product->id)}}">{{$product->product->name}}</h4>
                        <p>
                        @php
                            $currency = $product->product->currency_id ? $product->product->currency_id:1;
                            $sign = App\Models\Currency::find($currency);
                        @endphp
                        @if($sign->sign != "MMK")
                        {{$sign->sign." ".$product->product->price}}
                        @else
                        {{$product->product->price}} {{$sign->sign}}
                        @endif
                        </p>
                    </div>
                </div>
                </a>
            </div>
            @endforeach
        </div>
        {{ $products->links('vendor.pagination.custom') }}
      
    </div>
</section>
<!-- Latest Products End -->
</main>
@endsection