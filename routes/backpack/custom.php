<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('size', 'SizeCrudController');
    Route::crud('coloe', 'ColoeCrudController');
    Route::crud('color', 'ColorCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('catalogue', 'CatalogueCrudController');
    Route::crud('subcategory', 'SubcategoryCrudController');
    Route::get('product/report','ProductreportCrudController@report')->name('Preport');
    Route::post('product/excel','ProductreportCrudController@excel')->name('Excelreport');
    Route::get('dashboard',function(){
        return redirect('admin/product');
    });
    Route::crud('productreport', 'ProductreportCrudController');
    Route::crud('currency', 'CurrencyCrudController');
}); // this should be the absolute last line of this file