@extends(backpack_view('blank'))
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=PT+Sans&display=swap" rel="stylesheet">

<style>
    .date_btn {
        background-color: #fff !important;
        border: 1px solid #aaa !important;
        border-radius: 4px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        margin-top: 6px !important;
    }

    .select-lang .select2-container .select2-selection--single .select2-selection__rendered {
        margin-top: 0 !important;
    }

    .select2-container--default .select2-selection--multiple {
        min-height: 40px !important;
    }

    .select2-container .select2-selection--single {
        height: 40px !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__arrow {
        top: 8px !important;
    }
</style>
@section('header')
    <div class="container-fluid">
        <div class="row d-flex justify-content-between">
            <h2>
                <span class="text-capitalize">
                    Product Excel Report
                </span>
            </h2>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <form action="{{ url('/admin/product/excel') }}" method="post">
            @csrf
            <div class="col-md-12">
                <div style="padding-bottom: 20px; padding-top: 20px;">
                    <div class="pt-3 pl-2">
                        <p>Please Click the below button to download product report with excel format.</p>
                        <button class="btn btn-danger" style="border-radius: 0;" id="get-report">
                            <i class='nav-icon la la-download'></i>
                            DOWNLOAD EXCEL REPORT
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('after_scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
@endpush
