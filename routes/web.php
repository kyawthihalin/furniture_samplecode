<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.index');
});
Route::get('/contact-us', function () {
    return view('pages.contact-us');
});
Route::get('/designer', function () {
    return view('pages.designer');
});
Route::get('/about-us', function () {
    return view('pages.about-us');
});
Route::get('/showcase', function () {
    return view('pages.showcase');
});
Route::get('/service', function () {
    return view('pages.service');
});
Route::get('/brand', function () {
    return view('pages.brand');
});
Route::get('/factory', function () {
    return view('pages.factory');
});
// Route::get('/{slug}','Frontend\CategoryController@getCategory')->name('category');
Route::get('sub/{slug}','Frontend\CategoryController@getSubcategory')->name('subcategory');
Route::get('single-product/{id}','Frontend\CategoryController@getSingleProduct')->name('single');

