@extends('../layout.app')
@section('main')

<main>
    <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h4 class="text-center">Contact us</h4>
                    <div class="contact-sub pt-5">GENERAL ENQUIRES</div>
                    <p>Please feel free to contact us at any time using the form below.</p>
                    <p>Alternatively, you can contact us on +95 1 581 185 or at sales@enchantmm.com and we will be in touch as soon as possible.</p>
                </div>

               <!--  <div class="col-12">
                    <div class="contact-sub pt-5">CLAIMS</div>
                    
                        <p>Please email us at sales@enchantmm.com</p>

                        <p>Your email should include the following information so that we can promptly resolve the issues:</p>

                        <p>Yours or your customer's name</p>

                        <p> The sales invoice / delivery document number</p>

                        <p>Your own photographs of the damage from close up and from a distance to indicate perspective.</p>

                        <p>We will then be in touch with you directly to facilitate a swift and satisfactory solution.</p>
                </div> -->
            </div>
            <div class="row">
                <div class="col-12 col-md-6 pt-5">
                    <div class="contact-sub pb-3">Melbourne Showroom</div>
                    <ul>
                        <li> Mon - Fri : 9 am – 6 pm </li>
                        <li> Saturday : 9 am to 5 pm </li>
                        <li> Sunday : 9 am to 5 pm </li>
                        <li> No. 753/754, Pinlon Road, 30 Quarter, North Dagon Township, Yangon, Myanmar, 11421 </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
   
    <!-- ================ contact section start ================= -->
        <section class="contact-section">
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-12">
                        <h2 class="contact-title text-center">Get in Touch</h2>
                    </div>
                    <div class="col-lg-8">
                        <form class="form-contact contact_form" action="contact_process.php" method="post" id="contactForm" novalidate="novalidate">
                            <div class="row">
                               
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Name*</label>
                                        <input class="form-control valid" name="name" id="name" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your name'" placeholder="Enter your name">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="email">Email*</label>
                                        <input class="form-control valid" name="email" id="email" type="email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'" placeholder="Email">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="phone">Phone*</label>
                                        <input class="form-control" name="phone" id="phone" type="text" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Phone Number'" placeholder="Enter Phone Number">
                                    </div>
                                </div>
                                 <div class="col-12">
                                    <div class="form-group">
                                        <label for="Message">Message*</label>
                                        <textarea class="form-control w-100" name="message" id="message" cols="30" rows="9" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Message'" placeholder=" Enter Message"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-3 text-center">
                                <button type="submit" class="button button-contactForm boxed-btn">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    <!-- ================ contact section end ================= -->
</main>

@endsection