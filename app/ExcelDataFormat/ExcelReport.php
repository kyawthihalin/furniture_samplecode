<?php

namespace App\ExcelDataFormat;

use App\Models\Currency;

class ExcelReport
{
    public static function specificType($products)
    {
        $download_data = [];
        $count = 0;
     
        foreach ($products as $product) {
            $status = "Active";
            $colors = "";
            if(count($product->colors) > 0)
            {
                foreach($product->colors as $color){
                    if($colors != "" || $colors!= null)
                    {
                        $colors = $colors.",".$color->name;
                    }
                    else
                    {
                        $colors = $color->name;
                    }
                }
            }
            $sizes = "";
            if(count($product->sizes) > 0)
            {
                foreach($product->sizes as $size){
                    if($sizes != "" || $sizes!= null)
                    {
                        $sizes = $sizes.",".$size->name;
                    }
                    else
                    {
                        $sizes = $size->name;
                    }

                }
            }
            
            if($product->status == 0){
                $status = "Inactive";
            }

            $subs = "";
            if(count($product->subcategories) > 0)
            {
                foreach($product->subcategories as $subcat){
                    if($subs != "" || $subs != null)
                    {
                        $subs = $subs.",".$subcat->name;
                    }
                    else
                    {
                        $subs = $subcat->name;
                    }
                }
            }
            
            if($product->status == 0){
                $status = "Inactive";
            }

            $currency = Currency::find($product->currency_id);
            $download_data[$count]['Product ID'] = $product->product_uid;
            $download_data[$count]['Product Name'] = $product->name;
            $download_data[$count]['Active/Inactive'] = $status;
            $download_data[$count]['Stock Balance'] = $product->in_stock? $product->in_stock : 0;
            $download_data[$count]['Currency'] = $currency->name;
            $download_data[$count]['Price'] = $product->price? $product->price : 0;
            $download_data[$count]['Available Colors'] = $colors;
            $download_data[$count]['Available Sizes'] = $sizes;
            $download_data[$count]['Sub-Categories'] = $subs;
            $download_data[$count]['Created At'] = $product->created_at;
            $count++;
        }
        return $download_data;
    }
}
