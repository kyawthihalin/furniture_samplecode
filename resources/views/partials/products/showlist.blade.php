<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }

</style>
@if(count($products) > 0)
<table>
    <thead>
        <th>No</th>
        <th>Product ID</th>
        <th>Product Name</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Product Status</th>
        <th>Available Colors</th>
        <th>Sizes</th>
    </thead>
    
    <tbody>
    @php
        $count = 1;
        $total = 0;
    @endphp
    @foreach($products as $product)
    @php
        $status = "Active";
        if($product->status == 0)
        {
            $status = "Inactive";
        }
        $currency = App\Models\Currency::find($product->currency_id);
    @endphp
    <tr>
        <td>{{$count++}}</td>
        <td>{{$product->product_uid}}</td>
        <td>{{$product->name}}</td>
        <td>{{$currency->sign}} {{$product->price? number_format($product->price,0):0}}</td>
        <td>{{$product->in_stock ? number_format($product->in_stock,0):0}}</td>
        <td>{{$status}}</td>
        <td>
            @foreach($product->colors as $color)
               <span class="badge badge-pill" style="background-color:{{$color->color_code}}">{{$color->name}}</span>
                @if(!$loop->last)
                    <span>,</span>
                @endif
            @endforeach
        </td>
        <td>
            @foreach($product->sizes as $size)
               <span class="badge badge-pill badge-secondary">{{$size->name}}</span>
                @if(!$loop->last)
                    <span>,</span>
                @endif
            @endforeach
        </td>

    </tr>
    @endforeach
    </tbody>
  
</table>
@else
    <p>Sorry, There is no Data.</p>
@endif