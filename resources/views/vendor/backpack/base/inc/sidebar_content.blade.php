<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@if(backpack_user()->can('list-product'))
<li class="nav-title">Manage Products</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon las la-archive'></i> Products</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('productreport') }}'><i class='nav-icon la la-download'></i> Download Excel</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('productreport/create') }}'><i class='nav-icon la la-print'></i> Product Report</a></li>
@endif

@if(backpack_user()->can('list-category') || backpack_user()->can('list-size') || backpack_user()->can('list-color') || backpack_user()->can('list-catalogue'))
<li class="nav-title">Product Settings</li>
<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-tools"></i>Settings</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list-category'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon las la-list'></i> Categories</a></li>
        @endif
        @if(backpack_user()->can('list-subcategory'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('subcategory') }}'><i class='nav-icon las la-list'></i> Sub Category</a></li>
        @endif
        @if(backpack_user()->can('list-size'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('size') }}'><i class='nav-icon las la-sitemap'></i> Sizes</a></li>
        @endif
        @if(backpack_user()->can('list-color'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('color') }}'><i class='nav-icon la la-palette'></i> Colors</a></li>
        @endif
        @if(backpack_user()->can('list-catalogue'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('catalogue') }}'><i class='nav-icon las la-book-open'></i>Upload Catalogues</a></li>
        @endif    
        @if(backpack_user()->can('list-currency'))
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('currency') }}'><i class='nav-icon las la-dollar-sign'></i> Currencies</a></li>
        @endif    
    </ul>
</li>
@endif

@if(backpack_user()->can('list-role') || backpack_user()->can('list-user') || backpack_user()->can('list-permission'))
<li class="nav-title">Manage Authentication</li>
<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-user-shield"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        @if(backpack_user()->can('list-user'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user-check"></i> <span>Users</span></a></li>
        @endif
        @if(backpack_user()->can('list-role'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        @endif
        @if(backpack_user()->can('list-permission'))
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
        @endif
    </ul>
</li>
@endif
