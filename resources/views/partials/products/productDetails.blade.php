<?php
use App\Models\ProductSize;
use App\Models\ProductCategory;
use App\Models\ProductSubcategory;
use App\Models\ProductColor;
use App\Models\Color;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Size;

if($product->id)
{
    $product_sizes = ProductSize::where('product_id',$product->id)->get();
    $product_colors = ProductColor::where('product_id',$product->id)->get();
    $product_categories = ProductCategory::where('product_id',$product->id)->get();
    $product_subcategories = ProductSubcategory::where('product_id',$product->id)->get();
}
?>

@if($product != null)
    
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">Product Name</th>
                    <th scope="col">Product Colors</th>
                    <th scope="col">Product SubCategories</th>
                    <th scope="col">Product Sizes</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-success">
                  <td>
                      {{$product->name}}
                  </td>
                  @if($product_colors)
                  <td>
                      @foreach($product_colors as $product_color)
                        @php
                            $color = Color::find($product_color->color_id);
                        @endphp
                        @if($color != null)
                        <span style="background-color:{{ $color->color_code }}" class="badge badge-pill">{{$color->name}}</span>
                        @else
                        @endif
                    @endforeach
                  </td>
                  @endif
                  {{--@if($product_categories)
                  <td>
                      @foreach($product_categories as $product_category)
                        @php
                            $category = Category::find($product_category->category_id);
                        @endphp
                        <span style="background-color:white" class="badge badge-pill">{{$category->name}}</span>
                      @endforeach
                  </td>
                  @endif --}}
                  @if($product_subcategories)
                  <td>
                      @foreach($product_subcategories as $product_subcategory)
                        @php
                            $sub_category = Subcategory::find($product_subcategory->subcategory_id);
                        @endphp
                        @if($sub_category != null)
                            <span style="background-color:white" class="badge badge-pill">{{$sub_category->name}}</span>
                        @else
                        @endif
                      @endforeach
                  </td>
                  @endif
                  @if($product_sizes)
                  <td>
                      @foreach($product_sizes as $product_size)
                        @php
                            $size = Size::find($product_size->size_id);
                        @endphp
                        @if($size != null)
                        <span style="background-color:white" class="badge badge-pill">{{$size->name}}</span>
                        @else
                        @endif
                      @endforeach
                  </td>
                  @endif
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-md-12 p-0 m-0">
        <div class="col-md-12 mb-1 bg-secondary">Product Gallery</div>
        <div class="col-md-12">
            <div class="row">
            @foreach($product->image as $key => $image)
                <div class="col-md-3 mb-5 col-sm-10">
                    <a href="{{asset($image)}}" target="_blank" title="product image">
                        <img src="{{asset($image)}}" alt="product image" height="150px">
                    </a>
                </div>
            @endforeach
            </div>
        </div>
    </div>

       
@endif