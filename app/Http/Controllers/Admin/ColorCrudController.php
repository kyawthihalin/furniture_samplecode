<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ColorRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Helpers\Permission;

/**
 * Class ColorCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ColorCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Color::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/color');
        CRUD::setEntityNameStrings('color', 'colors');
        Permission::setPermissions('color',$this->crud);
        CRUD::denyAccess(['delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('name');
        CRUD::addColumn([
            'name'     => 'color_code',
            'label'    => 'Color',
            'type'     => 'closure',
            'function' => function($entry) {
                return '<i class="las la-square" style="color:'.$entry->color_code.';font-size:20px"></i>';
            }
        ]);
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ColorRequest::class);
        CRUD::field('name');
        CRUD::addField(
            [   // color_picker
                'label'                => 'Background Color',
                'name'                 => 'color_code',
                'type'                 => 'color_picker',
                'default'              => '#000000',
                'color_picker_options' => [
                    'customClass' => 'custom-class',
                    'horizontal' => true,
                    'extensions' => [
                        [
                            'name' => 'swatches',
                            'options' => [
                                'colors' => [
                                    'primary' => '#337ab7',
                                    'success' => '#5cb85c',
                                    'info' => '#5bc0de',
                                    'warning' => '#f0ad4e',
                                    'danger' => '#d9534f'
                                ],
                                'namesAsValues' => false
                            ]
                        ]
                    ]
                ]
            ]
        );
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
